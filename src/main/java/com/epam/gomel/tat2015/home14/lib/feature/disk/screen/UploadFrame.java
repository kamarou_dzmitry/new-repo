package com.epam.gomel.tat2015.home14.lib.feature.disk.screen;

import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class UploadFrame {

    public static By closeButton = By.xpath("//button[contains(@class,'button-close')]");
    public static By doneIcon = By.xpath("//div[@class='b-item-upload__icon b-item-upload__icon_done']");
    public static By frame = By.xpath("//div[@class='_nb-popup-i']");

    public DiskBasePage clickCloseButton() {
        Browser.current().click(closeButton);
        return new DiskBasePage();
    }

}
