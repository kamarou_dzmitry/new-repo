package com.epam.gomel.tat2015.home14.lib.feature.disk.service;

import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.home14.lib.util.Logger;

public class DiskLoginService {

    public void loginToDisk(Account account) {
        Logger.debug("Login to disk with");
        Logger.debug("Login name: '" + account.getLogin() + "'");
        Logger.debug("Login password: '" + account.getPassword() + "'");
        new MailLoginService().loginToMailbox(account).switchToDiskBasePage();
    }

}
