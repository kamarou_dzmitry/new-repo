package com.epam.gomel.tat2015.home14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class MailboxMenu {

    By sentLink = By.xpath("//a[@href='#sent']");
    By inboxLink = By.xpath("//a[contains(text(),'Inbox')]");
    By trashLink = By.xpath("//a[@href='#trash']");

    public InboxPage openInbox() {
        Browser.current().click(inboxLink);
        return new InboxPage();
    }

    public SentPage openSent() {
        Browser.current().click(sentLink);
        return new SentPage();
    }

    public TrashPage openTrash() {
        Browser.current().click(trashLink);
        return new TrashPage();
    }

    public SpamPage openSpam() {
        return new SpamPage();
    }

    public DraftsPage openDrafts() {
        return new DraftsPage();
    }

}
