package com.epam.gomel.tat2015.home14.lib.runner;

import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import org.testng.*;

public class CustomTestNgListener extends TestListenerAdapter {

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        Logger.info("[Invoked method]: " + iInvokedMethod.getTestMethod().getMethodName(), iTestResult.getThrowable());
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        Logger.info("[Method invocation finished]", iTestResult.getThrowable());
    }

    public void onStart(ISuite iSuite) {
        Logger.info("[Start the suite]: " + iSuite.getName());
    }

    public void onFinish(ISuite iSuite) {
        Logger.info("[Suite is finished]: " + iSuite.getName());
    }

    public void onTestStart(ITestResult iTestResult) {
        Logger.info("[Test started]: " + iTestResult.getName(), iTestResult.getThrowable());
    }

    public void onTestSuccess(ITestResult iTestResult) {
        Logger.info("[Test success with result]: " + iTestResult);
    }

    public void onTestFailure(ITestResult iTestResult) {
        Browser.current().screenshot(" fail");
        Logger.info("[Test fail with result]: " + iTestResult);
        Logger.info("[Screenshot was created]");
    }

    public void onTestSkipped(ITestResult iTestResult) {
        Logger.info("[Test skipped with result]: " + iTestResult);
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }

    public void onStart(ITestContext iTestContext) {
    }

    public void onFinish(ITestContext iTestContext) {
        Browser.current().getWrappedDriver().close();
    }

}
