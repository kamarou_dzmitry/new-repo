package com.epam.gomel.tat2015.home14.tests.mail;

import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.Letter;
import com.epam.gomel.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteDraftMailTest {

    MailLoginService mailLoginService = new MailLoginService();
    Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.buildLetter();
    MailService mailService = new MailService();

    @Test(description = "Checking deleted letter in trash and deleting it permanently")
    public void deleteDraftMailToTrashAndPermanently() {
        mailLoginService.loginToMailbox(account);
        mailService.sendLetter(letter);
        mailService.deleteLetterFromInbox(letter);
        Assert.assertEquals(mailService.isLetterExistsInTrash(letter), true, "Check deleted mail in trash fail");
        mailService.deleteLetterFromTrash(letter);
    }

}
