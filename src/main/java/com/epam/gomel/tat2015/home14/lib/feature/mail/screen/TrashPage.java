package com.epam.gomel.tat2015.home14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.home14.lib.ui.Browser;

public class TrashPage extends MailboxBasePage {

    public TrashPage() {
        Browser.current().click(new MailboxMenu().trashLink);
    }

}
