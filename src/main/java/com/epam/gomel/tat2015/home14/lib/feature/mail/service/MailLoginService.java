package com.epam.gomel.tat2015.home14.lib.feature.mail.service;

import com.epam.gomel.tat2015.home14.lib.feature.mail.screen.InboxPage;
import com.epam.gomel.tat2015.home14.lib.feature.mail.screen.LoginFailedPage;
import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.mail.screen.MailLoginPage;
import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import com.epam.gomel.tat2015.home14.lib.util.Logger;

public class MailLoginService {

    public InboxPage loginToMailbox(Account account) {
        Logger.debug("Login to mail with");
        Logger.debug("Login name: '" + account.getLogin() + "'");
        Logger.debug("Login password: '" + account.getPassword() + "'");
        Browser.current().screenshot();
        return MailLoginPage.open().login(account.getLogin(), account.getPassword());
    }

    public void checkSuccessLogin(Account account) {
        Logger.debug("Checking success login");
        String address = MailLoginPage.open().login(account.getLogin(), account.getPassword()).getCurrentAddress();
        if (account == null || !address.equals(account.getEmail())) {
            throw new RuntimeException("Login to mailbox failed. Login = " +
                    account.getLogin() + ", Password = " + account.getPassword() + ". Current email = " + address);
        }
    }

    public boolean retrieveError(Account account) {
        Logger.debug("Retrieving error");
        MailLoginPage.open().login(account.getLogin(), account.getPassword());
        String message = new LoginFailedPage().getErrorMassage();
        if (message != null) {
            Logger.debug("Error message: " + message);
            Browser.current().screenshot();
            return true;
        }
        return false;
    }

}
