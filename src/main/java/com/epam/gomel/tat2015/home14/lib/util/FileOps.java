package com.epam.gomel.tat2015.home14.lib.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class FileOps {

    public static String getFileContent(String filePath) {
        String fileContent = "";
        while (fileContent.equals("")) {
            try {
                fileContent = FileUtils.readFileToString(new File(filePath));
            } catch (IOException e) {
                Logger.error("Read file to string exception", e);
                break;
            }
        }
        return fileContent;
    }

}
