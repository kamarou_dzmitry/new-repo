package com.epam.gomel.tat2015.home14.lib.feature.disk.service;

import com.epam.gomel.tat2015.home14.lib.config.GlobalConfig;
import com.epam.gomel.tat2015.home14.lib.feature.common.CommonConstants;
import com.epam.gomel.tat2015.home14.lib.feature.disk.screen.DiskBasePage;
import com.epam.gomel.tat2015.home14.lib.feature.disk.screen.TrashPage;
import com.epam.gomel.tat2015.home14.lib.feature.disk.screen.UploadFrame;
import com.epam.gomel.tat2015.home14.lib.ui.Browser;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;

public class DiskService {

    public void uploadFile(File file) {
        Logger.debug("Upload the file");
        Logger.debug("File path: '" + file.getAbsolutePath() + "'");
        new DiskBasePage().typeToUpload(file.getAbsolutePath());
        Browser browser = Browser.current();
        browser.waitForClickable(UploadFrame.closeButton);
        browser.click(UploadFrame.closeButton);
        browser.waitForInvisible(UploadFrame.frame);
    }

    public void uploadFiles(File[] files) {
        Logger.debug("Upload files:");
        for (File file : files) {
            uploadFile(file);
        }
    }

    public boolean isFileUploaded(File file) {
        Logger.debug("Checking is file '" + file.getName() + "' uploaded");
        goTo(CommonConstants.DISK_URL);
        Browser.current().screenshot();
        try {
            Browser.current().findElement(findFile(file.getName())).click();
            return true;
        } catch (Exception e) {
            Logger.error("File is not uploaded", e);
        }
        return false;
    }

    public boolean isFilesUploaded(File[] files) {
        Logger.debug("Checking files uploading:");
        goTo(CommonConstants.DISK_URL);
        for (File file : files) {
            if (!isFileUploaded(file)) {
                Logger.debug("File " + file.getName() + "is not loaded");
                return false;
            }
        }
        return true;
    }

    public void checkElementInTrash(String elementName) {
        Logger.debug("Checking element in the trash");
        goTo(CommonConstants.TRASH_URL);
        Browser.current().screenshot();
        Browser.current().click(By.xpath(String.format("//div[@data-id='/trash/%s']", elementName)));
    }

    public boolean checkElementsInTrash(File[] files) {
        Logger.debug("Checking elements in the trash:");
        goTo(CommonConstants.TRASH_URL);
        try {
            for (File file : files) {
                Browser.current().click(By.xpath(String.format("//div[@data-id='/trash/%s']", file.getName())));
            }
        } catch (Exception e) {
            Logger.error("Element isn't in trash", e);
            return false;
        }
        return true;
    }

    public By findFile(String fileName) {
        Logger.debug("Finding file");
        return Browser.current().findElement(String.format("//div[@title='%s']", fileName));
    }

    public boolean downloadSelectedFile(File file) {
        Logger.debug("Downloading selected file");
        Browser.current().click(By.xpath(String.format("//button[contains(@data-params,'" + file.getName() + "')][2]")));
        boolean downloaded = false;
        File downloadedFile = new File(GlobalConfig.config().getTempDirectory() + "\\Downloads\\" + file.getName());
        Logger.debug("Downloading file...");
        while (!downloaded) {
            downloaded = downloadedFile.exists();
        }
        Logger.debug("Download complete");
        return true;
    }

    public void deleteByDragAndDrop(String fileName) {
        Logger.debug("Deleting by drag&drop");
        new DiskBasePage().selectElement(fileName).moveToTrash(fileName);
    }

    public void deleteSelected(String fileName) {
        Logger.debug("Deleting selected");
        new DiskBasePage().moveToTrash(fileName);
    }

    public WebElement deleteFileFromTrash(String fileName) {
        Logger.debug("Deleting file " + fileName + " from trash");
        checkElementInTrash(fileName);
        Browser.current().click(By.xpath(String.format("//button[contains(@data-params,'%s')][2]", fileName)));
        Browser.current().screenshot();
        return Browser.current().findElement(TrashPage.itemMovedMessage);
    }

    public void goTo(String folderURL) {
        Logger.debug("Loading page " + folderURL);
        if (!(Browser.current().getCurrentURL()).equals(folderURL)) {
            Browser.current().getWrappedDriver().get(folderURL);
        }
        Browser.current().screenshot();
    }

    public void restoreFile(String fileName) {
        Logger.debug("Restoring file " + fileName);
        goTo(CommonConstants.TRASH_URL);
        new DiskBasePage().selectElement(fileName);
        Browser.current().click(TrashPage.restoreButton);
    }

    public void moveSelectedFilesToTrash(File[] files) {
        Logger.debug("Moving selected files to the trash");
        goTo(CommonConstants.DISK_URL);
        selectSeveralFiles(files);
        deleteSelected(files[0].getName());
    }

    public void selectSeveralFiles(File[] files) {
        Logger.debug("Selecting several files:");
        Browser.current().open(CommonConstants.DISK_URL);
        Browser.current().ctrlDown();
        By[] diskElementsPaths = new By[files.length];
        WebElement[] diskElements = new WebElement[files.length];
        for (int i = 0; i < files.length; i++) {
            diskElementsPaths[i] = By.xpath(String.format("//div[@data-id='/disk/%s']", files[i].getName()));
            diskElements[i] = Browser.current().findElement(diskElementsPaths[i]);
            diskElements[i].click();
        }
        Browser.current().ctrlUp();
        Browser.current().screenshot();
    }

}
