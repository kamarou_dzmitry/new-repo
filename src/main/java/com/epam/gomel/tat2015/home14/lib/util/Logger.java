package com.epam.gomel.tat2015.home14.lib.util;

public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam.gomel.tat2015.home14");

    public static void info(String message) {
        logger.info(message);
    }

    public static void info(Object message, Throwable t) {
        logger.info(message, t);
    }

    public static void error(Object message) {
        logger.error(message);
    }

    public static void error(String message, Throwable t) {
        logger.error(message, t);
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void debug(Object message, Throwable t) {
        logger.debug(message, t);
    }

    public static void fatal(Object message) {
        logger.fatal(message);
    }

    public static void fatal(Object message, Throwable t) {
        logger.fatal(message, t);
    }

    public static void warn(Object message) {
        logger.warn(message);
    }

    public static void warn(Object message, Throwable t) {
        logger.warn(message, t);
    }

}
