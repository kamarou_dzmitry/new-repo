package com.epam.gomel.tat2015.home14.lib.util;

import java.util.Scanner;

public class Utils {

    public static String cutID(String attribute) {
        Scanner scanner = new Scanner(attribute);
        scanner.useDelimiter("/");
        String id = "";
        while (scanner.hasNext()) {
            id = scanner.next();
        }
        return id;
    }

}
