package com.epam.gomel.tat2015.home14.lib.feature.disk.screen;

import org.openqa.selenium.By;

public class TrashPage {

    public static By trashMark = By.xpath("//div[@class='b-crumbs__item b-crumbs__item_folding']");
    public static By itemMovedMessage = By.xpath("//div[@class='notifications__item nb-island notifications__item_moved']");
    public static By restoreButton = By.xpath("//button[@data-click-action='resource.restore']");

}
