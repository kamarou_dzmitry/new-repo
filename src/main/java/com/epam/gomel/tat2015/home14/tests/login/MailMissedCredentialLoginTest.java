package com.epam.gomel.tat2015.home14.tests.login;

import com.epam.gomel.tat2015.home14.lib.feature.common.Account;
import com.epam.gomel.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.feature.common.CommonConstants;
import com.epam.gomel.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.home14.lib.util.Randomizer;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MailMissedCredentialLoginTest {

    private MailLoginService mailLoginService = new MailLoginService();
    private Account account;

    @Test(description = "Check expected error message in case of non existed account or wrong password", dataProvider = "dp1")
    public void checkErrorMessageNonExistedAccount(String login, String password, boolean displayed) {
        account = AccountBuilder.getNewAccount(login, password);
        boolean isDisplayed = mailLoginService.retrieveError(account);
        Assert.assertEquals(isDisplayed, displayed, "Check error message non existed account fail");
    }

    @DataProvider(name = "dp1")
    public Object[][] dataProvider() {
        return new Object[][]{
                {CommonConstants.DEFAULT_USER_LOGIN, Randomizer.numeric(), true},
                {Randomizer.numeric(), CommonConstants.DEFAULT_USER_PASSWORD, true},
                {Randomizer.numeric(), Randomizer.numeric(), true}
        };
    }

}
