package com.epam.gomel.tat2015.home14.lib.config;

import com.epam.gomel.tat2015.home14.lib.ui.BrowserType;
import com.epam.gomel.tat2015.home14.lib.util.Logger;
import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.kohsuke.args4j.Option;
import org.testng.xml.XmlSuite.ParallelMode;

import java.util.List;

public class GlobalConfig {

    private static GlobalConfig instance;

    @Option(name = "-brt", aliases = {"-browser", "-browser_type", "-bt", "--browser_type"}, usage = "browser type: firefox or chrome")
    private BrowserType browserType = BrowserType.CHROME;

    @Option(name = "-sui", aliases = {"-suites", "-suite", "-s", "--suites"}, usage = "list of paths to suites",
            handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites = null;

    @Option(name = "-prm", aliases = {"-parallel", "-parallel_mode", "-pm"}, usage = "parallel mode: false or tests")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-thc", aliases = {"-threads", "--thread_count", "-thread_count", "-tc"},
            usage = "amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    @Option(name = "-hos", aliases = {"-host", "-hostname", "-selenium_host", "-hub"}, usage = "selenium host, example: 'http://127.0.0.1'")
    private String seleniumHub = "";

    @Option(name = "-por", aliases = {"-port"}, usage = "selenium port", depends = {"-hos"})
    private int seleniumPort = 4444;

    /*@Option(name = "-chd", aliases = {"-chromedriver", "-chromeDriver", "-chrome_driver", "-chrome_Driver"},
            usage = "chrome web driver")
    private String chromeDriver = "c:/chromedriver_win.exe";*/

    @Option(name = "-tmd", aliases = {"-tempDirectory", "-result_dir"}, usage = "temp directory that will be created")
    private String tempDir = FileUtils.getTempDirectory().getAbsolutePath() + "\\Temp";

    public static GlobalConfig config() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public void setBrowserType(BrowserType browserType) {
        Logger.debug("Set browser type=" + browserType);
        this.browserType = browserType;
    }

    public BrowserType getBrowserType() {
        Logger.debug("Get browser type=" + browserType);
        return browserType;
    }

    public void setSuites(List<String> suites) {
        Logger.debug("Set suites=" + suites);
        this.suites = suites;
    }

    public List<String> getSuites() {
        Logger.debug("Get suites=" + suites);
        return suites;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        Logger.debug("Set parallel mode=" + parallelMode);
        this.parallelMode = parallelMode;
    }

    public ParallelMode getParallelMode() {
        Logger.debug("Get parallel mode=" + parallelMode);
        return parallelMode;
    }

    public void setThreadCount(int threadCount) {
        Logger.debug("Set thread count=" + threadCount);
        this.threadCount = threadCount;
    }

    public int getThreadCount() {
        Logger.debug("Get thread count=" + threadCount);
        return threadCount;
    }

    public void setSeleniumHost(String seleniumHost) {
        Logger.debug("Set host=" + seleniumHost);
        this.seleniumHub = seleniumHost;
    }

    public String getSeleniumHost() {
        Logger.debug("Get host=" + seleniumHub);
        return seleniumHub;
    }

    public void setSeleniumPort(int seleniumPort) {
        Logger.debug("Set port=" + seleniumPort);
        this.seleniumPort = seleniumPort;
    }

    public int getSeleniumPort() {
        Logger.debug("Get port=" + seleniumPort);
        return seleniumPort;
    }

    /*public void setChromeDriver(String chromeDriver) {
        Logger.debug("Set chrome driver=" + chromeDriver);
        this.chromeDriver = chromeDriver;
    }

    public String getChromeDriver() {
        Logger.debug("Get chrome driver=" + chromeDriver);
        return chromeDriver;
    }*/

    public void setTempDirectory(String tempDir) {
        Logger.debug("Set temp directory=" + tempDir);
        this.tempDir = tempDir;
    }

    public String getTempDirectory() {
        Logger.debug("Get temp directory=" + tempDir);
        return tempDir;
    }

}


